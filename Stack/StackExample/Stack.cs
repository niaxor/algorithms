﻿using System;

namespace StackExample
{
    public class Stack<T>
    {
        private const int DefaultSize = 2;

        public int Size => index;

        private T[] data;
        private int index = 0;

        public Stack()
        {
            data = new T[DefaultSize];
        }

        public void Push(T item)
        {
            if (item == null)
                throw new ArgumentException("Can't push null.");

            EnsureCapacity(Size + 1);
            data[index] = item;
            index++;
        }

        private void EnsureCapacity(int size)
        {
            if (data.Length < size)
            {
                var newArray = new T[data.Length * 2];
                Array.Copy(data, newArray, data.Length);
                data = newArray;
            }
        }

        public T Pop()
        {
            if (Size == 0)
                throw new InvalidOperationException("Stack is empty.");

            index--;
            var value = data[index];
            return value;
        }

        public T Peek()
        {
            if (Size == 0)
                throw new InvalidOperationException("Stack is empty.");

            return data[Math.Max(index - 1, 0)];
        }
    }
}
