﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StackExample.Tests
{
    [TestClass]
    public class StackTests
    {
        Stack<string> victim = new Stack<string>();
        private const string TestString_1 = "1001";

        [TestMethod]
        public void Push_SizeIsZero_SizeIsOne()
        {
            victim.Push(TestString_1);
            Assert.AreEqual(victim.Size, 1);
        }

        [TestMethod]
        public void Push_SizeIsZero_SizeIsTen()
        {
            for (int i = 0; i < 10; i++)
            {
                victim.Push(TestString_1);
            }

            Assert.AreEqual(victim.Size, 10);
        }

        [TestMethod]
        public void Push_ItemIsNull_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                victim.Push(null);
            });
        }

        [TestMethod]
        public void Pop_SizeIsOne_SizeIsZero()
        {
            victim.Push(TestString_1);
            victim.Pop();
            Assert.AreEqual(victim.Size, 0);
        }

        [TestMethod]
        public void Pop_SizeIsTen_SizeIsZero()
        {
            for (int i = 0; i < 10; i++)
            {
                victim.Push(TestString_1);
            }
            for (int i = 0; i < 10; i++)
            {
                victim.Pop();
            }
            Assert.AreEqual(victim.Size, 0);
        }

        [TestMethod]
        public void Pop_ValueAsExpected()
        {
            victim.Push(TestString_1);
            Assert.AreEqual(TestString_1, victim.Pop());
        }

        [TestMethod]
        public void Pop_SizeIsZero_ThrowsInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                victim.Pop();
            });
        }

        [TestMethod]
        public void Peek_ValueAsExpected()
        {
            victim.Push(TestString_1);
            Assert.AreEqual(victim.Peek(), TestString_1);
        }

        [TestMethod]
        public void Peek_SizeIsZero_InvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                victim.Peek();
            });
        }

        [TestMethod]
        public void Peek_SizeIsOne_SizeIsOne()
        {
            victim.Push(TestString_1);
            victim.Peek();
            Assert.AreEqual(victim.Size, 1);
        }
    }
}