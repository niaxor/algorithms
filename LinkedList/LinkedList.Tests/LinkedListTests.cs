﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinkedListExample;
using System;

namespace LinkedList.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        public void PushFront_SizeIsZero_SizeIsOne()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.PushFront(1);
            Assert.AreEqual(linkedList.Size, 1);
        }

        [TestMethod]
        public void PushFront_SizeIsZero_SizeIsTen()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            for (int i = 0; i < 10; i++)
            {
                linkedList.PushFront(i);
            }

            Assert.AreEqual(linkedList.Size, 10);
        }

        [TestMethod]
        public void PushBack_SizeIsZero_SizeIsOne()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.PushBack(1);
            Assert.AreEqual(linkedList.Size, 1);
        }

        [TestMethod]
        public void PushBack_SizeIsZero_SizeIsTen()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            for (int i = 0; i < 10; i++)
            {
                linkedList.PushBack(i);
            }

            Assert.AreEqual(linkedList.Size, 10);
        }

        [TestMethod]
        public void Get_ReturnsExpectedValue_1()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.PushFront(15);
            Assert.AreEqual(linkedList.Get(0), 15);
        }

        [TestMethod]
        public void Get_ReturnsExpectedValue_2()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.PushBack(14);
            linkedList.PushFront(15);
            Assert.AreEqual(linkedList.Get(0), 15);
        }

        [TestMethod]
        public void Remove_SizeIsOne_SizeIsZero()
        {
            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.PushFront(1);
            linkedList.Remove(1);
            Assert.AreEqual(linkedList.Size, 0);
        }

        [TestMethod]
        public void Remove_ItemDoesNotExistInList_ArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                LinkedList<string> linkedList = new LinkedList<string>();
                linkedList.PushFront("test");
                linkedList.Remove(null);
            });
        }
    }
}