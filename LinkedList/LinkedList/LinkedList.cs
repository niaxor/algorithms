﻿using System;

namespace LinkedListExample
{
    public class LinkedList<T>
    {
        private const string ValueNotFoundException = "Value not found.";

        public int Size { get; private set; } = 0;

        private LinkedListNode<T> RootNode;
        private LinkedListNode<T> FinalNode;

        public void PushFront(T value)
        {
            if (RootNode == null)
            {
                RootNode = new LinkedListNode<T>(value);
                FinalNode = RootNode;
            }
            else
            {
                RootNode = new LinkedListNode<T>(value, RootNode);
            }

            Size++;
        }

        public void PushBack(T value)
        {
            if (FinalNode == null)
            {
                RootNode = new LinkedListNode<T>(value);
                FinalNode = RootNode;
            }
            else
            {
                FinalNode.Next = new LinkedListNode<T>(value);
                FinalNode = FinalNode.Next;
            }

            Size++;
        }

        public T Get(int index)
        {
            if (index >= Size)
            {
                throw new ArgumentException("Index is outside of list range.");
            }
            var currentNode = RootNode;

            for (int i = 0; i < index; i++)
            {
                currentNode = currentNode.Next;
            }

            return currentNode.Data;
        }

        public void Remove(T value)
        {
            if (RootNode == null)
                throw new ArgumentException(ValueNotFoundException);

            var currentNode = RootNode;
            LinkedListNode<T> previousNode = null;
            do
            { 
                // Found the value
                if (currentNode.Data.Equals(value))
                {
                    if (currentNode == FinalNode)
                    {
                        FinalNode = previousNode;
                    }
                    if (previousNode != null)
                    {
                        previousNode.Next = currentNode.Next;
                    }
                    else
                    {
                        RootNode = currentNode.Next;
                    }

                    Size--;
                    return;
                }

                previousNode = currentNode;
                currentNode = currentNode.Next;
            }
            while (currentNode != null);

            throw new ArgumentException(ValueNotFoundException);
        }
    }
}