﻿namespace LinkedListExample
{
    public class LinkedListNode<T>
    {
        public LinkedListNode<T> Next { get; set; }
        public T Data { get; set; }

        public LinkedListNode(T Data, LinkedListNode<T> Next = null)
        {
            this.Data = Data;
            this.Next = Next;
        }
    }
}
