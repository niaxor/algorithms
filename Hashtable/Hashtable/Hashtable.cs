﻿using System;
using System.Collections.Generic;

namespace HashtableExample
{
    /// <summary>
    /// An example of a Hashtable implementation using array-list seperate chaining for collision resolution
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class Hashtable<TKey, TValue>
    {
        private const int DefaultSize = 2;
        private const float LoadFactor = 0.7f;

        private int count = 0;
        private List<KeyValuePair<TKey, TValue>>[] buckets = new List<KeyValuePair<TKey, TValue>>[DefaultSize];

        public Hashtable()
        {
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new List<KeyValuePair<TKey, TValue>>();
            }
        }
        
        public void Add(TKey key, TValue value)
        {
            if (HasKey(key))
                throw new ArgumentException("Key already exists.");
            EnsureLoadFactorMaintained(count + 1);
            int bucketIndex = key.GetHashCode() % buckets.Length;
            buckets[bucketIndex].Add(new KeyValuePair<TKey, TValue>(key, value));
            count++;
        }

        public TValue GetValue(TKey key)
        {
            int index = key.GetHashCode() % buckets.Length;
            foreach (var item in buckets[index])
            {
                if (item.Key.Equals(key))
                    return item.Value;
            }

            throw new ArgumentException("Key does not exist.");
        }

        public void Remove(TKey key)
        {
            int index = key.GetHashCode() % buckets.Length;
            for (int i = 0; i < buckets[index].Count; i++)
            {
                if (buckets[index][i].Key.Equals(key))
                {
                    buckets[index].RemoveAt(i);
                    count--;
                    return;
                }
            }
            
            throw new ArgumentException("Key does not exist.");
        }

        public bool HasKey(TKey key)
        {
            int index = key.GetHashCode() % buckets.Length;
            foreach (var item in buckets[index])
            {
                if (item.Key.Equals(key))
                    return true;
            }

            return false;
        }

        private void EnsureLoadFactorMaintained(int itemCount)
        {
            if (itemCount > LoadFactor * buckets.Length)
            {
                IncreaseSize();
            }
        }

        private void IncreaseSize()
        {
            int newBucketCount = NextPrime((int)Math.Ceiling(LoadFactor * buckets.Length) - 1);
            List<KeyValuePair<TKey, TValue>>[] newBuckets = new List<KeyValuePair<TKey, TValue>>[newBucketCount];
            for (int i = 0; i < newBuckets.Length; i++) newBuckets[i] = new List<KeyValuePair<TKey, TValue>>();
            for (int i = 0; i < buckets.Length; i++)
            {
                if (newBuckets[i] == null)
                    continue;
                foreach (var kvp in newBuckets[i])
                {
                    int newBucketIndex = kvp.Key.GetHashCode() % buckets.Length;
                    newBuckets[newBucketIndex].Add(kvp);
                }
            }
            buckets = newBuckets;
        }

        private static int NextPrime(int start)
        {
            int number = start;
            if (number <= 1) return 2;
            if (number % 2 == 0)
            {
                number++;
                if (IsPrime(number)) return number;
            }
            
            do
            {
                number += 2;
            }
            while (!IsPrime(number));

            return number;
        }

        private static bool IsPrime(int number)
        {
            if (number <= 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }
    }
}
