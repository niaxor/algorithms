﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HashtableExample;

namespace HashtableExample.Tests
{
    [TestClass]
    public class HashtableTests
    {
        [TestMethod]
        public void HasKey_KeyNotPresent_KeyExists()
        {
            Hashtable<int, int> hashtable = new Hashtable<int, int>();
            hashtable.Add(1, 1);
            Assert.IsTrue(hashtable.HasKey(1));
        }

        [TestMethod]
        public void AddKey_KeyExists_ArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                Hashtable<int, int> hashtable = new Hashtable<int, int>();
                hashtable.Add(1, 1);
                hashtable.Add(1, 1);
            }
            );
        }

        [TestMethod]
        public void GetValue_IsExpectedValue()
        {
            Hashtable<int, int> hashtable = new Hashtable<int, int>();
            hashtable.Add(1, 1);
            Assert.AreEqual(hashtable.GetValue(1), 1);
        }

        [TestMethod]
        public void Remove_HasKey_DoesNotHaveKey()
        {
            Hashtable<int, int> hashtable = new Hashtable<int, int>();
            hashtable.Add(1, 1);
            hashtable.Remove(1);
            Assert.IsFalse(hashtable.HasKey(1));
        }
    }
}
