﻿using ArrayListExample;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArrayList.Tests
{
    [TestClass]
    public class ArrayListTests
    {
        [TestMethod]
        public void Add_Empty_CountIsOne()
        {
            ArrayList<int> array = new ArrayList<int>();
            array.Add(1337);
            Assert.AreEqual(array.Count, 1);
        }

        [TestMethod]
        public void Remove_CountIsOne_CountIsZero()
        {
            ArrayList<int> array = new ArrayList<int>();
            array.Add(0);
            array.Remove(0);
            Assert.AreEqual(array.Count, 0);
        }

        [TestMethod]
        public void Get_ReturnsExpectedValue()
        {
            ArrayList<int> array = new ArrayList<int>();
            array.Add(1337);
            int value = array.Get(0);
            Assert.AreEqual(1337, value);
        }
    }
}
