﻿using System;

namespace ArrayListExample
{
    public class Program
    {
        static void Main(string[] args)
        {
            ArrayList<int> set = new ArrayList<int>();
            for (int i = 0; i < 10; i++)
            {
                set.Add(i + 1);
            }

            for (int i = 0; i < set.Count; i++)
            {
                Console.WriteLine(set.Get(i));
            }

            Console.ReadLine();
        }
    }
}
