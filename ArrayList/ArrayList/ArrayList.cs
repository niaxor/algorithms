﻿using System;

namespace ArrayListExample
{
    public class ArrayList<T>
    {
        private const int DefaultSize = 4;

        public int Count { get => index; }

        private T[] items = new T[DefaultSize];
        private int index = 0;

        public ArrayList() { }

        public ArrayList(T[] items)
        {
            this.items = items;
            index = items.Length;
        }

        public void Add(T item)
        {
            EnsureItemSize(index+1);
            items[index++] = item;
        }

        public T Get(int index)
        {
            return items[index];
        }

        private void EnsureItemSize(int count)
        {
            if (count >= items.Length)
            {
                T[] newItems = new T[items.Length * 2];
                Array.Copy(items, newItems, index);
                items = newItems;
            }
        }

        public void Remove(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (items[i].Equals(item))
                {
                    for (int j = i; j < Count - 1; j++)
                    {
                        items[j] = items[j + 1];
                    }
                    index--;
                    return;
                }
            }
        }
    }
}