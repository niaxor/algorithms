﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QueueExample;

namespace QueueExample.Tests
{
    [TestClass]
    public class QueueTests
    {
        public Queue<int> victim = new Queue<int>();

        [TestMethod]
        public void Enqueue_Empty_SizeIsOne()
        {
            
            victim.Enqueue(1001);
            Assert.AreEqual(victim.Size, 1);
        }

        [TestMethod]
        public void Enqueue_Empty_SizeIsTen()
        {
            for (int i = 0; i < 10; i++)
            {
                victim.Enqueue(i);
            }

            Assert.AreEqual(victim.Size, 10);
        }

        [TestMethod]
        public void Dequeue_SizeIsOne_SizeIsZero()
        {
            victim.Enqueue(1001);
            victim.Dequeue();
            Assert.AreEqual(victim.Size, 0);
        }

        [TestMethod]
        public void Dequeue_SizeIsTen_SizeIsZero()
        {
            for (int i = 0; i < 10; i++)
            {
                victim.Enqueue(i);
            }

            for (int i = 0; i < 10; i++)
            {
                victim.Dequeue();
            }
            Assert.AreEqual(victim.Size, 0);
        }

        [TestMethod]
        public void Dequeue_ExpectedValue()
        {
            victim.Enqueue(1001);
            int value = victim.Dequeue();
            Assert.AreEqual(1001, value);
        }

        [TestMethod]
        public void Dequeue_SizeIsZero_ThrowsInvalidOperationException()
        {
            Assert.ThrowsException<System.InvalidOperationException>(() =>
            {
                victim.Dequeue();
            });
        }
    }
}
