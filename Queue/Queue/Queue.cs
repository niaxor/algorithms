﻿using System;

namespace QueueExample
{
    public class Queue<T>
    {
        private QueueNode<T> entryNode;

        public int Size { get; private set; }

        public void Enqueue(T item)
        {
            if (entryNode == null)
            {
                entryNode = new QueueNode<T>(item);
            }
            else
            {
                var currentNode = entryNode;
                while (currentNode.Next != null) 
                {
                    currentNode = currentNode.Next;
                }

                currentNode.Next = new QueueNode<T>(item);
            }

            Size++;
        }

        public T Dequeue()
        {
            if (entryNode == null)
                throw new InvalidOperationException("The queue is empty.");
            var value = entryNode.Value;
            entryNode = entryNode.Next;
            Size--;

            return value;
        }
    }
}
