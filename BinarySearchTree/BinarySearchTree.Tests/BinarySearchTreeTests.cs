﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BinarySearchTreeExample;

namespace BinarySearchTreeExample.Tests
{
    [TestClass]
    public class BinarySearchTreeTests
    {
        BinarySearchTree victim;

        [TestInitialize]
        public void Initialise()
        {
            victim = new BinarySearchTree();
        }

        [TestMethod]
        public void Add_DoesNotExist_Exists()
        {
            victim.Add(1);
            Assert.IsTrue(victim.Exists(1));
        }

        [TestMethod]
        public void Add_SizeIsZero_SizeIsOne()
        {
            victim.Add(1);
            Assert.AreEqual(victim.Size, 1);
        }

        [TestMethod]
        public void Add_DoesNotExist_DoesNotExist()
        {
            victim.Add(1);
            victim.Add(2);
            victim.Add(3);
            Assert.IsFalse(victim.Exists(4));
        }
    }
}