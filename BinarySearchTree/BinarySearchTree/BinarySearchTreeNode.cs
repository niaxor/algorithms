﻿namespace BinarySearchTreeExample
{
    public class BinarySearchTreeNode
    {
        public int Value { get; set; }
        public BinarySearchTreeNode LeftNode { get; set; }
        public BinarySearchTreeNode RightNode { get; set; }

        public BinarySearchTreeNode(int value)
        {
            Value = value;
        }
    }
}
