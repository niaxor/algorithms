﻿using System;

namespace BinarySearchTreeExample
{
    public class BinarySearchTree
    {
        public BinarySearchTreeNode RootNode { get; set; }
        public int Size { get; private set; }

        public void Add(int value)
        {
            if (RootNode == null)
            {
                RootNode = new BinarySearchTreeNode(value);
                Size++;
                return;
            }

            var currentNode = RootNode;
            while (currentNode != null)
            {
                // Go left
                if (value <= currentNode.Value)
                {
                    // Left node doesn't exist
                    if (currentNode.LeftNode == null)
                    {
                        currentNode.LeftNode = new BinarySearchTreeNode(value);
                        break;
                    }
                    // Left node exists
                    else
                    {
                        currentNode = currentNode.LeftNode;
                    }
                }
                // Go right
                if (value > currentNode.Value)
                {
                    // Right node doesn't exist
                    if (currentNode.RightNode == null)
                    {
                        currentNode.RightNode = new BinarySearchTreeNode(value);
                        break;
                    }
                    // Right node exists
                    else
                    {
                        currentNode = currentNode.RightNode;
                    }
                }
            }

            Size++;
        }

        public bool Exists(int value)
        {
            return FindValue(value, RootNode);
        }

        private bool FindValue(int value, BinarySearchTreeNode currentNode)
        {
            if (currentNode == null)
                return false;

            if (currentNode.Value == value)
                return true;

            if (value <= currentNode.Value)
            {
                return FindValue(value, currentNode.LeftNode);
            }
            else
            {
                return FindValue(value, currentNode.RightNode);
            }
        }
    }
}