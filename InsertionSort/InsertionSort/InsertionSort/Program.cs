﻿using System;

namespace InsertionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input = ReadInput();
            InsertionSort(ref input);
            PrintOutput(input);

            Console.ReadLine();
        }
        
        static void InsertionSort(ref int[] data)
        {
            for (int i = 1; i < data.Length; i++)
            {
                int key = data[i];
                int j = i - 1;
                while (j >= 0 && data[j] > key)
                {
                    data[j + 1] = data[j];
                    j--;
                }
                data[j + 1] = key;
            }
        }

        static int[] ReadInput()
        {
            Console.Write("Input: ");
            var keys = Console.ReadLine().Split(new string[] { ", ", "," }, StringSplitOptions.RemoveEmptyEntries);
            return Array.ConvertAll<string, int>(keys, s => int.Parse(s));
        }

        static void PrintOutput(int[] data)
        {
            string[] outputs = Array.ConvertAll<int, string>(data, i => i.ToString());
            string output = string.Join(", ", outputs);
            Console.WriteLine("Output = ({0})", output);
        }
    }
}